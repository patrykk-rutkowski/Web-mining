import requests
import xml.etree.ElementTree as ET
import py7zlib
from os import listdir
from os.path import isfile, join
import json

dataPath = "STACK/"
dataCountToAnalyze = 5

headers = {'X-AYLIEN-TextAPI-Application-Key': '07cec01b5917c2a79a2cb8c1e263752c', 'X-AYLIEN-TextAPI-Application-ID': '51bbe6f4'}
url = 'https://api.aylien.com/api/v1/sentiment'

onlyfiles = [f for f in listdir(dataPath) if isfile(join(dataPath, f))]
questions = []

def writeStats(value, valueConfidence, type):
    if (value == 0):
        print("Wypowiedzi " + type + ": brak")
    else:
        print("Wypowiedzi " + type + ": " + str(value))
        print("Stopień wierności: " + str(round(float(valueConfidence)/value*100, 2)) + "%\n")

print("------------------------------------------\n")

for file1 in onlyfiles:
    questions = []
    fp = open(dataPath + file1, 'rb')
    archive = py7zlib.Archive7z(fp)
    posts = ET.fromstring(archive.getmember('Posts.xml').read().decode())
    coments = ET.fromstring(archive.getmember('Comments.xml').read().decode())
    i = 0

    for i in range(0, dataCountToAnalyze):
        postId = posts[i].get('Id')
        value = posts[i].get('Body')
        for coment in coments:
            if (coment.get('PostId') == postId):
                value += coment.get('Text')
        questions.append(value)

    negative = 0
    positive = 0
    neutral = 0
    positiveConfidence = 0
    negativeConfidence = 0
    neutralConfidence = 0

    
    for question in questions:
        payload = {'mode': 'document', 'text': question, 'language': 'en'}
        r = requests.post(url, data=payload, headers=headers)
        resp_dict = json.loads(r.text)
        if (resp_dict['polarity'] == 'positive'):
            positive = positive + 1
            positiveConfidence = positiveConfidence + resp_dict['polarity_confidence']
        elif (resp_dict['polarity'] == 'negative'):
            negative = negative + 1
            negativeConfidence = negativeConfidence + resp_dict['polarity_confidence']
        elif (resp_dict['polarity'] == 'neutral'):
            neutral = neutral + 1
            neutralConfidence = neutralConfidence + resp_dict['polarity_confidence']
    
    print("Dane dla " + file1)
    print("Przeanalizowanych pytań: " + str(dataCountToAnalyze) + "\n")
    writeStats(positive, positiveConfidence, "pozytywnych")
    writeStats(negative, negativeConfidence, "negatywnych")
    writeStats(neutral, neutralConfidence, "neutralnych")
    print("------------------------------------------\n\n")