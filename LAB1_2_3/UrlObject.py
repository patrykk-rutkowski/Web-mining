class UrlObject:

    mainUrl: str
    urls = []
    depth: int
    visitedLink: bool

    def __init__(self, mainUrl, depth, visitedLink = False):
        self.mainUrl = mainUrl
        self.depth = depth

    def __repr__(self):
        return self.mainUrl