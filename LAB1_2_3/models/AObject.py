class AObject:

    url: str
    checkDepth: bool
    depth: int
    parent: str

    def __init__(self, url, depth, parent, checkDepth):
        self.url = url
        self.depth = depth
        self.parent = parent
        self.checkDepth = checkDepth

    def __repr__(self):
        return "Rodzic: " + self.parent + " dziecko: " + self.url + " " + str(self.depth) + "\n"