class ResultObject:
    url: str
    words = {}
    sublinks = []

    def __init__(self, url, words, sublinks):
        self.url = url
        self.words = words
        self.sublinks = sublinks