from bs4 import BeautifulSoup
from models.AObject import AObject
from UrlObject import UrlObject
from anytree import Node, RenderTree
import operator
import re
import validators
import urllib.request
import scipy
import scipy.spatial


class SOUPUtils:

    @staticmethod
    def getLinksFromHTML(html):
        links = []
        soup = BeautifulSoup(html, 'html.parser')
        inputTag = soup.findAll('a') 
        for tag in inputTag:
            if (tag.has_attr("href") and validators.url(tag['href'])):
                links.append(tag['href'])
        return links


    @staticmethod
    def getLinksScriptTML(html):
        scripts = []
        soup = BeautifulSoup(html, 'html.parser')
        inputTag = soup.findAll('script') 
        for tag in inputTag:
            if (tag.has_attr("src")):
                scripts.append(tag['src'])
        return scripts

    @staticmethod
    def getImgScriptTML(html):
        imgs = []
        soup = BeautifulSoup(html, 'html.parser')
        inputTag = soup.findAll('img')
        for tag in inputTag:
            if (tag.has_attr("src")):
                imgs.append(tag['src'])
        return imgs

    @staticmethod
    def getWordsFromHTML(html, wordRankingDictionary, db_ref):
        wordRankingDictionary = {}
        sortedRanking = []
        soup = BeautifulSoup(html, 'html.parser')
        [s.extract() for s in soup(['style', 'script', '[document]', 'head', 'title'])]
        visible_text = soup.getText()

        for match in re.findall(r'[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]{2,}', visible_text, re.MULTILINE):
            if not match.lower() in wordRankingDictionary.keys():
                wordRankingDictionary[match.lower()] = 1
            else:
                wordRankingDictionary[match.lower()] += 1
        sortedRanking = sorted(wordRankingDictionary.items(), key=operator.itemgetter(1), reverse=True)
        print(sortedRanking)

        db_ref.set({"words": wordRankingDictionary})

        return sortedRanking

    @staticmethod
    def readHtml(url):
        if (validators.url(url)):
            try:
                opener = urllib.request.FancyURLopener({})
                f = opener.open(url)
                return f.read()
            except:
                return ""
        else:
            ""
    @staticmethod
    def calculateCos(words1, words2):
        vec1 = []
        vec2 = []
        setWords1 = words1.copy()
        setWords2 = words2.copy()

        for key, value in setWords1.items():
            vec1.append(value)
            if (key in setWords2.keys()):
                vec2.append(value)
                setWords2.pop(key)
            else:
                vec2.append(0)
        
        for key, value in setWords2.items():
            vec2.append(value)
            if (key in setWords1.keys()):
                vec1.append(value)
            else:
                vec1.append(0)

        if (len(vec1) < len(vec2)):
            for i in range (len(vec1), len(vec2)):
                vec1.append(0)
        else:
            for i in range (len(vec2), len(vec1)):
                vec2.append(0)

        return (1.0 - scipy.spatial.distance.cosine(vec1, vec2))*100

    @staticmethod
    def getSublinks(link, depth):
        depth = depth - 1
        setLinks = set()
        links = []
        connections = {}
        links.append(AObject(link, depth, "", False))
        depth -= 1
        while (depth >= 0):
            for value in list(links):
                if (value.checkDepth == False):
                    parent = value.url
                    value.checkDepth = True
                    content = SOUPUtils.readHtml(value.url)
                    test = set()

                    if not value.url in connections.keys():
                        connections[value.url] = []
                        addToAll = True
                    else:
                        addToAll = False

                    for u in SOUPUtils.getLinksFromHTML(content):
                        if addToAll:
                            connections[value.url].append(u)
                        if (u not in setLinks):
                            test.add(u)
                            setLinks.add(u)

                    for val in test:
                        links.append(AObject(val, depth, parent, False))
            depth -= 1

        return links, connections