import validators

class ConsoleAttrs:

    depth: int = 1
    printInConsole = False
    saveToFile = False
    writeText = False
    writeHref = False
    writeScriptLink = False
    writeImgLink = False
    cos = False
    graph = False
    urls = []

    def __init__(self, argv):
        for parameter in argv:
            if (parameter == '-console'):
                self.printInConsole = True
                continue
    
            if (parameter == '-file' and sys.argv[4] != ''):
                self.saveToFile = True
                continue

            if (parameter == '-text'):
                self.writeText = True
                continue

            if (parameter == '-a'):
                self.writeHref = True
                continue

            if  (parameter == '-script'):
                self.writeScriptLink = True
                continue

            if (parameter == '-img'):
                self.writeImgLink = True
                continue

            if (parameter == '-depth'):
                if (len(argv) >= argv.index(parameter) + 1):
                    self.depth = int(argv[argv.index(parameter) + 1])

            if (parameter == '-url'):
                index = int(argv.index(parameter))

                for i in range(index + 1, len(argv)):
                    if (argv[i].startswith('-')):
                        break
                    else:
                        self.urls.append(argv[i])

            if (parameter == '-cos'):
                self.cos = True

            if (parameter == "-graph"):
                self.graph = True