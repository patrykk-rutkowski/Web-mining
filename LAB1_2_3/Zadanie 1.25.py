# -*- coding: utf-8 -*-
from SOUPUtils import SOUPUtils
from FileUtils import FileUtils
from ConsoleAttrs import ConsoleAttrs
from UrlObject import UrlObject
from ResultObject import ResultObject
from TextResult import TextResult
from urllib.parse import urlparse
import networkx as nx
import matplotlib.pyplot as plt
import sys
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

testValue = ConsoleAttrs(sys.argv)

results = []
linksWithSublinks = []
connections = []
for url in testValue.urls:
    t, v = SOUPUtils.getSublinks(url, testValue.depth)
    connections.append(v)
    results.append(ResultObject(url, {}, t))

if (len(results) > 0):

    cred = credentials.Certificate('firebase.json')
    firebase_admin.initialize_app(cred)
    db = firestore.client()

    for reasultObject in results:
        for parentArray in reasultObject.sublinks:
            
            dataParent = urlparse(parentArray.url)
            doc_ref = db.collection(u'websites').document(dataParent.netloc)

            print(parentArray)
            content = SOUPUtils.readHtml(parentArray.url)

            if (testValue.printInConsole):
                print(content)

            if (testValue.saveToFile):
                FileUtils.saveFile(sys.argv[4])

            if (testValue.writeImgLink):
                print(SOUPUtils.getImgScriptTML(content))

            if (testValue.writeScriptLink):
                print(SOUPUtils.getLinksScriptTML(content))

            if (testValue.writeHref):
                url.urls = UrlObject(SOUPUtils.getLinksFromHTML(content), testValue.depth)
                print(parentArray.urls.mainUrl)

            if (testValue.writeText or testValue.cos):
                print(parentArray.url)
                SOUPUtils.getWordsFromHTML(content, reasultObject.words, doc_ref)

if (testValue.cos and len(results) >= 2):
    i = 0
    while i < len(results):
        for j in reversed(range(i+1, len(results))):
            print("Porównianie: " + str(results[i].url) + " i " + str(results[j].url) + " " + "\nPodobieństwo stron: {0:.2f}".format(SOUPUtils.calculateCos(results[i].words, results[j].words)) + "%")
        i = i + 1





G = nx.DiGraph()
for v in connections:
    for value in v:
        dataParent = urlparse(value)
        G.add_node(dataParent.netloc)

        for z in v[value]:
            dataChildren = urlparse(z)
            G.add_node(dataChildren.netloc)
            G.add_edge(dataParent.netloc, dataChildren.netloc)

nx.draw(G,with_labels=True)
plt.show()

pr = nx.pagerank(G, alpha=0.9)
print(pr)
for url in pr:
    doc_ref = db.collection(u'websites').document(url)
    doc_ref.update({'pagerank': pr[url]})


if (testValue.graph):
    print(pr)