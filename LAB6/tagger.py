import re
import operator
import xml.etree.ElementTree as ET
import py7zlib
from os import listdir
from os.path import isfile, join
from textblob.classifiers import NaiveBayesClassifier

dataPath = "STACK/"

percentageDataToLearn = 5  #Najlepiej podać małą liczbę, żeby komputer się nie zawiesił
questionToAnalyze = 5        #Ile pytań przeanalizować

onlyfiles = [f for f in listdir(dataPath) if isfile(join(dataPath, f))]

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  cleantext = cleantext.replace("\n", "")
  return cleantext

def printBestTags(result):
    tagDict = {}
    result = cl.prob_classify(cleanhtml(posts[i + dataToTeachClasifier].get('Body')))
    for tag in result.samples():
        tagDict[tag] = result.prob(tag)

    tagDict = sorted(tagDict.items(), key=operator.itemgetter(1), reverse=True)
    for tag in tagDict[0:5]:
        print(tag[0] + ", " + str(tag[1]*100))

if (percentageDataToLearn > 100):
    exit

for file1 in onlyfiles:
    fp = open(dataPath + file1, 'rb')
    archive = py7zlib.Archive7z(fp)
    posts = ET.fromstring(archive.getmember('Posts.xml').read().decode())

    t = []
    trainfeats = []
    dataToTeachClasifier = int((len(posts)*percentageDataToLearn)/100)
    print("Do nauki pobrano: " + str(dataToTeachClasifier) + " pytań")
    for i in range(0, dataToTeachClasifier):
        question = cleanhtml(posts[i].get('Body'))
        originalTags = posts[i].get('Tags')
        if (originalTags is not None):
            originalTags = originalTags.replace("<", "").split(">")
            originalTags = originalTags[0:len(originalTags)-1]
            for tag in originalTags:
                trainfeats.append((question, tag))

    cl = NaiveBayesClassifier(trainfeats)

    for i in range(0, questionToAnalyze):
        print("\nPytanie: " + cleanhtml(posts[i + dataToTeachClasifier].get('Body')))
        print("Najbardziej pasujące tagi:")
        printBestTags(cl.prob_classify(cleanhtml(posts[i + dataToTeachClasifier].get('Body'))))